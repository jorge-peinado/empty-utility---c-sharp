An implementation in C sharp of the awesome old utility empty.exe, one of the best utilities on Windows Resource Kit.
The utility allows to empty the working set of a user process. If it is executed with administrative rights, it can empty the working set of any process running on a Windows system.
Usage:

empty.exe <pid>

Compilation:

C:\Windows\Microsoft.NET\Framework64\v4.0.30319\csc.exe empty.cs

Tested on Windows 8.1 and Windows 10 v1903