using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace TestPrograms
{
  class empty
  {
    [DllImport("psapi.dll")]
    public static extern bool EmptyWorkingSet(IntPtr hProcess);

    static int Main(string[] args) {
      if (args.Length == 0)
      {
        System.Console.WriteLine("Please enter a numeric argument.");
        return 1;
      }

      int num = Int32.Parse(args[0]);
      Console.WriteLine("Emptying working set of {0}", num);

      Process Proc = Process.GetProcessById(num);
      EmptyWorkingSet(Proc.Handle);

      return 0;
    }
  }
}
